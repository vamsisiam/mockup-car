import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetListComponent } from './reset-list.component';

describe('ResetListComponent', () => {
  let component: ResetListComponent;
  let fixture: ComponentFixture<ResetListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResetListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
